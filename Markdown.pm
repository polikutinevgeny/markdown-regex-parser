package Markdown;
use strict;
use warnings;

use Exporter qw(import);

our @EXPORT_OK = qw(toHTML);

my $escape_char = '\\';
my $ec = "(?<!\\$escape_char)"; #escape char look-behind regex
my $ec_pos = "\\$escape_char"; #escape char search regex

my $paragraphs = sub {
	my $par = shift;
	return "\n$par\n" if ($par =~ /(^<\/?(ul|ol|li|h|p|bl))/);
	return "\n<p>$par<\/p>\n\n";
};

my $unordered_list = sub {
	my ($input, $level) = @_;
	my $t = $$input =~ s/^\t{$level}$ec[*\-+.](.*)/("\t" x $level)."<ul><li>$1<\/li><\/ul>"/gme;
	return $t;
};

my $fix_unordered_list = sub {
	my ($list, $level) = @_;
	$list =~ s/(\<\/ul\>\n\t{$level}\<ul\>*)+//g;
	return $list;
};

my $ordered_list = sub {
	my ($input, $level) = @_;
	my $t = $$input =~ s/^\t{$level}$ec\d++\.(.*)/("\t" x $level)."<ol><li>$1<\/li><\/ol>"/gme;
	return $t;
};

my $fix_ordered_list = sub {
	my ($list, $level) = @_;
	$list =~ s/(\<\/ol\>\n\t{$level}\<ol\>*)+//g;
	return $list;
};

sub toHTML {
	my $input = shift;
	$input =~ s/$ec(#{1,6})\s?(.*)/'<h'.length($1).'>'.$2.'<\/h'.length($1).'>'/ge; #headers
	$input =~ s/$ec!$ec\[([^\[\n]+)$ec\]$ec\(([^\)\n]+)$ec\)/<img src=\'$2\' alt=\'$1\'>/g; #images
	$input =~ s/$ec\[([^\[\n]+)$ec\]$ec\(([^\)\n]+)$ec\)/<a href=\'$2\'>$1<\/a>/g; #links
	$input =~ s/$ec\[([^\[\n]+)$ec\]$ec\[([^\[\n]+)$ec\]/<a href=\'$2\'>$1<\/a>/g; #another link style
	while ($input =~ s/$ec\[([^\[\n]+)$ec\]: ([^\)\n]+)//) { # remove tags
		my ($tag, $link) = ($1, $2);
		$input =~ s/<a href=\'$tag\'>(.*)<\/a>/<a href=\'$link\'>$1<\/a>/g; # replace tags with links
	}
	$input =~ s/(($ec\*$ec\*)|($ec\_$ec\_))([^_\*\n]*?)$ec\1/<strong>$4<\/strong>/g; #strong text
	$input =~ s/($ec\*|$ec\_)(.*?)$ec\1/<em>$2<\/em>/g; #emphasised text
	$input =~ s/($ec\~$ec\~)([^\~\n]*?)$ec\1/<del>$2<\/del>/g; #deleted text
	$input =~ s/($ec\`){3}(.*?)($ec\`){3}/<pre><code>$2<\/code><\/pre>/gms; #preformatted text
	$input =~ s/($ec\`)([^\`\n]*?)$ec\1/<code>$2<\/code>/g; #monospace text
	$input =~ s/(^$ec> )(.*)/<blockquote>$2<\/blockquote>/gm; #quote text
	$input =~ s/<\/blockquote>\n<blockquote>/\n/g; #fix quotes
	$input =~ s/($ec-){3,}(.*)/<hr>/g; #horizontal rule
	# start unordered list parsing
	my $level = 0;
	while ($unordered_list->(\$input, $level++)) {	}
	for (0 .. $level - 1) {
		$input =~ s/((^\t{$_}[^\t\n]+\n)+)/$fix_unordered_list->($1, $_)/gme;
	}
	for (1 .. $level - 1) {
		$input =~ s/<\/ul>\n(\t{$_})<ul>/\n$1<ul>/gm;
	}
	for (1 .. $level - 1) {
		$input =~ s/^(\t{$_}[^\t]*)<\/ul>\n<ul>/$1.("<\/ul>" x $_)/gme;
		$input =~ s/^(\t{$_}[^\t]*)<\/ul>\n^$/$1.("<\/ul>" x ($_ + 1))/gme;
	}
	# start ordered list parsing
	$level = 0;
	while ($ordered_list->(\$input, $level++)) {	}
	for (0 .. $level - 1) {
		$input =~ s/((^\t{$_}[^\t\n]+\n)+)/$fix_ordered_list->($1, $_)/gme;
	}
	for (1 .. $level - 1) {
		$input =~ s/<\/ol>\n(\t{$_})<ol>/\n$1<ol>/gm;
	}
	for (1 .. $level - 1) {
		$input =~ s/^(\t{$_}[^\t]*)<\/ol>\n<ol>/$1.("<\/ol>" x $_)/gme;
		$input =~ s/^(\t{$_}[^\t]*)<\/ol>\n^$/$1.("<\/ol>" x ($_ + 1))/gme;
	}
	$input =~ s/  \n/<br>/g; #line breaks
	$input =~ s/(^\n)(((.+)\n?)*)$/$paragraphs->($2)/gme; #paragraphs
	$input =~ s/<p><\/p>//g; #fix empty paragraphs
	$input =~ s/$ec_pos(.{1})/$1/g; # unescape escaped chars
	return $input;
}

1;