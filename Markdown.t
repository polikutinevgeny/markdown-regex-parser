use strict;
use Test::More;
use Devel::Cover;
use Markdown qw(toHTML);

subtest 'Headers' => sub {
	cmp_ok(toHTML('# header1'), 'eq', '<h1>header1</h1>', 'Header 1');
	cmp_ok(toHTML('## header2'), 'eq', '<h2>header2</h2>', 'Header 2');
	cmp_ok(toHTML('### header3'), 'eq', '<h3>header3</h3>', 'Header 3');
	cmp_ok(toHTML('#### header4'), 'eq', '<h4>header4</h4>', 'Header 4');
	cmp_ok(toHTML('##### header5'), 'eq', '<h5>header5</h5>', 'Header 5');
	cmp_ok(toHTML('###### header6'), 'eq', '<h6>header6</h6>', 'Header 6');
	cmp_ok(toHTML('####### header7'), 'eq', '<h6># header7</h6>', 'Header 7(header 6 in fact)');
	cmp_ok(toHTML('\# not a header1'), 'eq', '# not a header1', 'not a header1');
	cmp_ok(toHTML('\#\# not a header2'), 'eq', '## not a header2', 'not a header2');
	cmp_ok(toHTML('\#\#\# not a header3'), 'eq', '### not a header3', 'not a header3');
	cmp_ok(toHTML('\#\#\#\# not a header4'), 'eq', '#### not a header4', 'not a header4');
	cmp_ok(toHTML('\#\#\#\#\# not a header5'), 'eq', '##### not a header5', 'not a header5');
	cmp_ok(toHTML('\#\#\#\#\#\# not a header6'), 'eq', '###### not a header6', 'not a header6');
	cmp_ok(toHTML('\## header1'), 'eq', '#<h1>header1</h1>', 'header 1');
	cmp_ok(toHTML('#\# header1'), 'eq', '<h1># header1</h1>', 'header 1');
};

subtest 'Images' => sub {
	cmp_ok(toHTML('![Image alt](Img URL)'), 'eq', '<img src=\'Img URL\' alt=\'Image alt\'>', 'Image');
	cmp_ok(toHTML('\![Image alt](Img URL)'), 'eq', '!<a href=\'Img URL\'>Image alt</a>', 'Not an image'); # without '!' it becomes a link 
	cmp_ok(toHTML('!\[Image alt](Img URL)'), 'eq', '![Image alt](Img URL)', 'Not an image');
	cmp_ok(toHTML('![Image alt\](Img URL)'), 'eq', '![Image alt](Img URL)', 'Not an image');
	cmp_ok(toHTML('![Image alt]\(Img URL)'), 'eq', '![Image alt](Img URL)', 'Not an image');
	cmp_ok(toHTML('![Image alt](Img URL\)'), 'eq', '![Image alt](Img URL)', 'Not an image');
};

subtest 'Links' => sub {
	cmp_ok(toHTML('[link](URL)'), 'eq', '<a href=\'URL\'>link</a>', 'Link');
	cmp_ok(toHTML('\[link](URL)'), 'eq', '[link](URL)', 'Not a link');
	cmp_ok(toHTML('[link\](URL)'), 'eq', '[link](URL)', 'Not a link');
	cmp_ok(toHTML('[link]\(URL)'), 'eq', '[link](URL)', 'Not a link');
	cmp_ok(toHTML('[link](URL\)'), 'eq', '[link](URL)', 'Not a link');
	cmp_ok(toHTML("[link][tag]\n[tag]: URL"), 'eq', "<a href=\'URL\'>link</a>\n", 'Link');
};

subtest 'Strong text' => sub {
	cmp_ok(toHTML('**bold**'), 'eq', '<strong>bold</strong>', 'Strong(asterisks)');
	cmp_ok(toHTML('__bold__'), 'eq', '<strong>bold</strong>', 'Strong(underline)');
	cmp_ok(toHTML('\**bold\**'), 'eq', '*<em>bold*</em>', 'not strong(asterisks)'); #emphasised in fact
	cmp_ok(toHTML('\__bold\__'), 'eq', '_<em>bold_</em>', 'not strong(underline)'); #emphasised in fact
	cmp_ok(toHTML('\**bold*\*'), 'eq', '*<em>bold</em>*', 'not strong(asterisks)'); #emphasised in fact
	cmp_ok(toHTML('\__bold_\_'), 'eq', '_<em>bold</em>_', 'not strong(underline)'); #emphasised in fact
	cmp_ok(toHTML('*\*bold*\*'), 'eq', '<em>*bold</em>*', 'not strong(asterisks)'); #emphasised in fact
	cmp_ok(toHTML('_\_bold_\_'), 'eq', '<em>_bold</em>_', 'not strong(underline)'); #emphasised in fact
	cmp_ok(toHTML('\*\*bold\*\*'), 'eq', '**bold**', 'not strong(asterisks)'); 
	cmp_ok(toHTML('\_\_bold\_\_'), 'eq', '__bold__', 'not strong(underline)');
};

subtest 'Emphasised text' => sub {
	cmp_ok(toHTML('*italics*'), 'eq', '<em>italics</em>', 'Italics(asterisks)');
	cmp_ok(toHTML('_italics_'), 'eq', '<em>italics</em>', 'Italics(underline)');
	cmp_ok(toHTML('\*italics\*'), 'eq', '*italics*', 'not italics(asterisks)');
	cmp_ok(toHTML('\_italics\_'), 'eq', '_italics_', 'not italics(underline)');
};

subtest 'Deleted text' => sub {
	cmp_ok(toHTML('~~deleted~~'), 'eq', '<del>deleted</del>', 'Deleted text');
	cmp_ok(toHTML('\~\~deleted\~\~'), 'eq', '~~deleted~~', 'Not deleted text');
};

subtest 'Monospace text' => sub {
	cmp_ok(toHTML('`code`'), 'eq', '<code>code</code>', 'Monospace text');
	cmp_ok(toHTML('\`code\`'), 'eq', '`code`', 'Not monospace text');
	cmp_ok(toHTML("```code\ncode\ncode```"), 'eq', "<pre><code>code\ncode\ncode</code></pre>", 'Monospace text');
	cmp_ok(toHTML("\\`\\`\\`code\ncode\ncode\\`\\`\\`"), 'eq', "```code\ncode\ncode```", 'Not monospace text');
};

subtest 'Quotes' => sub {
	cmp_ok(toHTML('> quote'), 'eq', '<blockquote>quote</blockquote>', 'One-line quote');
	cmp_ok(toHTML("> quote\n> quote"), 'eq', "<blockquote>quote\nquote</blockquote>", 'Multi-line quote');
	cmp_ok(toHTML('\> quote'), 'eq', '> quote', 'Not a quote');
};

subtest 'Unordered list' => sub {
	cmp_ok(toHTML("*apples\n*oranges\n*pears\n"), 'eq', "<ul><li>apples</li><li>oranges</li><li>pears</li></ul>\n", 'List(asterisks)');
	cmp_ok(toHTML("-apples\n-oranges\n-pears\n"), 'eq', "<ul><li>apples</li><li>oranges</li><li>pears</li></ul>\n", 'List(minus)');
	cmp_ok(toHTML("+apples\n+oranges\n+pears\n"), 'eq', "<ul><li>apples</li><li>oranges</li><li>pears</li></ul>\n", 'List(plus)');
	cmp_ok(toHTML(".apples\n.oranges\n.pears\n"), 'eq', "<ul><li>apples</li><li>oranges</li><li>pears</li></ul>\n", 'List(dot)');
	cmp_ok(toHTML("\\*apples\n*oranges\n*pears\n"), 'eq', "*apples\n<ul><li>oranges</li><li>pears</li></ul>\n", 'List(asterisks)');
	cmp_ok(toHTML("\\-apples\n-oranges\n-pears\n"), 'eq', "-apples\n<ul><li>oranges</li><li>pears</li></ul>\n", 'List(minus)');
	cmp_ok(toHTML("\\+apples\n+oranges\n+pears\n"), 'eq', "+apples\n<ul><li>oranges</li><li>pears</li></ul>\n", 'List(plus)');
	cmp_ok(toHTML("\\.apples\n.oranges\n.pears\n"), 'eq', ".apples\n<ul><li>oranges</li><li>pears</li></ul>\n", 'List(dot)');
	cmp_ok(toHTML("*apples\n\t*oranges\n\t*lemons\n*pears\n"), 'eq', "<ul><li>apples</li>\n\t<ul><li>oranges</li><li>lemons</li></ul><li>pears</li></ul>\n", 'Nested list(asterisks)');
	cmp_ok(toHTML("*apples\n*oranges\n\t*lemons\n\t*pears\n\n"), 'eq', "<ul><li>apples</li><li>oranges</li>\n\t<ul><li>lemons</li><li>pears</li></ul></ul>\n", 'Nested list(asterisks)');
};

subtest 'Ordered list' => sub {
	cmp_ok(toHTML("1.apples\n2.oranges\n10.pears\n"), 'eq', "<ol><li>apples</li><li>oranges</li><li>pears</li></ol>\n", 'List(numbers)');
	cmp_ok(toHTML("\\1.apples\n2.oranges\n10.pears\n"), 'eq', "1.apples\n<ol><li>oranges</li><li>pears</li></ol>\n", 'List(numbers)');
	cmp_ok(toHTML("1.apples\n\t2.oranges\n10.pears\n"), 'eq', "<ol><li>apples</li>\n\t<ol><li>oranges</li></ol><li>pears</li></ol>\n", 'Nested list(numbers)');
	cmp_ok(toHTML("1.apples\n2.oranges\n\t10.pears\n\n"), 'eq', "<ol><li>apples</li><li>oranges</li>\n\t<ol><li>pears</li></ol></ol>\n", 'Nested list(numbers)');
};

subtest 'Horizontal rule' => sub {
	cmp_ok(toHTML('---'), 'eq', '<hr>', 'Horizontal rule');
	cmp_ok(toHTML('\-\-\-'), 'eq', '---', 'Not a horizontal rule');
};

subtest 'Line breaks' => sub {
	cmp_ok(toHTML("one-two-three  \nfour"), 'eq', 'one-two-three<br>four', 'Line break');
	cmp_ok(toHTML("one-two-three\nfour"), 'eq', "one-two-three\nfour", 'No line break');
};

subtest 'Paragraphs' => sub {
	cmp_ok(toHTML("\none-two-three\n"), 'eq', "\n<p>one-two-three\n</p>\n\n", 'Paragraph');
};

subtest 'Misc' => sub {
	cmp_ok(toHTML('***it-bold***'), 'eq', '<em><strong>it-bold</strong></em>', 'italics-bold');
};

done_testing();

use File::Slurp qw(read_file write_file);

my $input = read_file('input.txt');
write_file('output.html', 
	'<html><head><link href="css.css" rel="stylesheet" type="text/css"/></head><body>'.
	toHTML($input).
	'</body></html>');